# Matomo

## Pages

- Website: https://matomo.org/
- Changelog: https://matomo.org/changelog/
- GitHub: https://github.com/matomo-org/matomo
- Docker Hub : https://hub.docker.com/_/matomo

## First install

Instead of using this image, use the official Docker image to have the wizard to create all required database tables.

Then, go to the Marketplace, search for the "Login OIDC" plugin and install it (required to setup the plugin database table).

After that you can switch to this image.

## Maintenance

All month, a new release of the [geolocation database](https://db-ip.com/db/download/ip-to-city-lite) is available.
You may configure the `LOCATION_DATABASE_PATH` argument in the Dockerfile.

Also check for plugin versions:

- https://plugins.matomo.org/EnvironmentVariables

A new Matomo release can happen; see following part for detailed instructions.

## Upgrading

The upgrading process is the following:

1. watch for a new release here: https://github.com/matomo-org/matomo/releases

2. wait that the Docker image is officially released on the [Docker Hub](https://hub.docker.com/_/matomo); happen a few days after the release on GitHub

3. from the web interface, do the migration

4. on this repository, change the tag in the Dockerfile, in the `FROM` instruction

5. replace the image used in production to the new image

## License

This program is free software and is distributed under [AGPLv3+ License](./LICENSE).
