FROM docker.io/library/matomo:4.10.1-apache

EXPOSE 80

# you will need to override these values
ENV PUBLIC_HOST localhost
ENV SALT iem3laingoja6AhmaW0reeh9lah1Aeve

WORKDIR /var/www/html
VOLUME /var/www/html

ARG LOCATION_DATABASE_PATH=https://download.db-ip.com/free/dbip-city-lite-2022-07.mmdb.gz

# install unzip
RUN apt-get update && apt-get install -y unzip gettext && rm -rf /var/lib/apt/lists/*

# download, unzip and install the EnvironmentVariables plugin
RUN curl -o EnvironmentVariables.zip \
  https://plugins.matomo.org/api/2.0/plugins/EnvironmentVariables/download/4.0.1 \
  && unzip EnvironmentVariables.zip \
  && rm EnvironmentVariables.zip \
  && mv EnvironmentVariables /usr/src/matomo/plugins

# copy local files into the image
COPY config.ini.php /config.ini.example.php
COPY init.sh /init.sh

RUN chmod +x /init.sh

# download location database
RUN curl -L "$LOCATION_DATABASE_PATH" | gunzip > /usr/src/matomo/misc/DBIP-City.mmdb

ENTRYPOINT ["/init.sh"]
CMD ["apache2-foreground"]
