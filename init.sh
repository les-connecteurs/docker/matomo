#!/bin/sh

set -e

echo "Initializing Matomo…"

# generate configuration files
envsubst \
  < /config.ini.example.php \
  > /usr/src/matomo/config/config.ini.php

if [ ! -e matomo.php ]; then
  tar cf - --one-file-system -C /usr/src/matomo . | tar xf -
  chown -R www-data .
fi

exec "$@"
